Pour : Informatique UTEC composé d'étudiants et d'administrateur

Qui veulent : Limiter l'accès des étudiants aux sites internet

Produit : CCProxy

Est un :  Serveur Proxy

Qui : 
- Gestion des autorisations des accès sur internet (URL HTTP) selon les groupes d'utilisateur


A la différence de : Firewall

Notre produit : Simple configuration/configuration modulable

# Risques

## Risques organisationels

Absence d'un collaborateur - fréquence faible - gravité haute

## Risques techniques

Déconnection - fréquence fiable - gravité haute

Non blocage de site - gravité haute 

## Risques externes (clients, utilisateurs…)

Néant 

## Risques gestion de projet





